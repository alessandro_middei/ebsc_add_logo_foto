module foto-add-logo

go 1.16

require (
	github.com/gookit/color v1.4.2
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	gopkg.in/yaml.v2 v2.4.0
)
