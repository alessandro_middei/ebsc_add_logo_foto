package main

import (
	"fmt"
	"image"
	"image/draw"
	"image/jpeg"
	"image/png"
	"io/ioutil"
	"os"
	"path/filepath"

	"gopkg.in/yaml.v2"

	"github.com/gookit/color"
	"github.com/nfnt/resize"
)

// Area di definizione delle costanti e dei messaggi di errore

const help = `

esempio di uso: foto-add-logo $immagine [position] [logo]

[position] se non definito sarà tr -> Top Right
  il primo valore è il posizionamento sull'asse y (Top / Center / Bottom)
  il secondo valore è il posizionamento sull'asse x (Laft / Center / Right)
          Matrice Valori
	       tl | tc | tr
	       cl | cc | cr
	       bl | bc | br

[logo] se non definito è quello presente nella cartella config
  altrimenti va indicato il full path e deve essere in formato png, verrà poi fatto il resize a paddingxpadding
`

//Config è il file di configurazione
type Config struct {
	OutputDir string `yaml:"output_foto"`
	Logo      string `yaml:"logo"`
}

func resizeImage(immagine string, max int) {
	srcFile, err := os.Open(immagine)
	if err != nil {
		color.Red.Println("Errore nell'aprire: %v", err)
	}
	src, err := png.Decode(srcFile)
	if err != nil {
		color.Red.Println("Errore nel render di: %v", err)
	}

	m := resize.Resize(uint(max), uint(max), src, resize.Lanczos3)

	out, err := os.Create("./tmp/resized_logo.png")
	if err != nil {
		color.Red.Println("Errore nel lavorare: %v", err)
	}
	defer out.Close()

	// write new image to file
	png.Encode(out, m)
}

func calculateMaxLogoDimension(x, y int) int {
	if x > y {
		max := (y / 3) - (y / 100 * 5)
		return max
	} else if x < y {
		max := (x / 3) - (x / 100 * 5)
		return max
	} else {
		max := (y / 3) - (y / 100 * 5)
		return max
	}
}

func calculateDimension(immagine string) (int, int) {
	reader, err := os.Open(immagine)
	if err != nil {
		fmt.Println("Errore nell'aprire la foto")
	}

	img, _, err := image.DecodeConfig(reader)
	if err != nil {
		return 0, 0 // handle error somehow
	}
	//fmt.Println("Dimensioni orginali: ", img.Width, img.Height)
	return img.Width, img.Height
}

func calculatelogoPosition(xDim, yDim, lDim int, posizione string) (int, int) {
	fullLogo := lDim
	halfLogo := lDim / 2
	padding := 30
	switch posizione {
	case "tl":
		//Calcoliamo il posizionamento togliendo le del bordo di padding px
		lxDim := padding
		lyDim := padding
		return lxDim, lyDim
	case "cl":
		//Calcoliamo il posizionamento dividendo l'immagine in 2
		// -> Se il risultato è pari facciamo che x è uguale diviso 2 - 150
		// -> Se il risultato è dispari facciamo che x è uguale diviso 2 -150 +1
		// per y facciamo y-padding per il bordo
		lyDim := yDim/2 - halfLogo
		lxDim := padding
		return lxDim, lyDim
	case "bl":
		//Calcoliamo il posizionamento togliendo dal bordo i padding px
		lxDim := padding
		lyDim := yDim - fullLogo - padding
		return lxDim, lyDim
	case "tc":
		lxDim := (xDim / 2) - halfLogo
		lyDim := padding
		return lxDim, lyDim
	case "cc":
		lxDim := (xDim / 2) - halfLogo
		lyDim := (yDim / 2) - halfLogo
		return lxDim, lyDim
	case "bc":
		lxDim := (xDim / 2) - halfLogo
		lyDim := yDim - (fullLogo + padding)
		return lxDim, lyDim
	case "tr":
		//Calcoliamo il posizionamento togliendo le del bordo di padding px
		//lxDim := int(float64(xDim)*pxtopoint) + padding
		lxDim := xDim - (fullLogo + padding)
		lyDim := padding
		return lxDim, lyDim
	case "cr":
		lyDim := (yDim / 2) - halfLogo
		lxDim := xDim - (fullLogo + padding)
		return lxDim, lyDim
	case "br":
		//Calcoliamo il posizionamento togliendo le dimensioni del logo e un bordo di padding px
		lxDim := xDim - (fullLogo + padding)
		lyDim := yDim - (fullLogo + padding)
		return lxDim, lyDim
	default:
		//Calcoliamo il posizionamento togliendo le dimensioni del logo e un bordo di padding px
		lxDim := int(float64(xDim)) - (fullLogo + padding)
		lyDim := int(float64(yDim)) - (fullLogo + padding)
		return lxDim, lyDim
	}
}

func verifyLogoDimension(x, y, max int) bool {
	if x >= max && y >= max {
		return false
	} else {
		return true
	}
}

func addWaterMark(immagine, logo, output string, xstart, ystart int) {

	srcFile, err := os.Open(immagine)

	if err != nil {
		color.Red.Println("Errore nell'aprire source: %v", err)
	}
	src, err := jpeg.Decode(srcFile)
	if err != nil {
		color.Red.Println("Errore nel render di: %v", err)
	}

	dstFile, err := os.Open(logo)
	if err != nil {
		color.Red.Println("Errore nell'aprire logo: %v", err)
	}
	dst, err := png.Decode(dstFile)
	if err != nil {
		color.Red.Println("Errore nel render di: %v", err)
	}

	offset := image.Pt(xstart, ystart)
	//fmt.Println("Offset: ", xstart, ystart)
	b := src.Bounds()
	image3 := image.NewRGBA(b)
	ZP := image.Point{0, 0}
	draw.Draw(image3, b, src, ZP, draw.Src)
	draw.Draw(image3, dst.Bounds().Add(offset), dst, ZP, draw.Over)

	outName := fmt.Sprintf("%s/%s", output, filepath.Base(immagine))
	third, err := os.Create(outName)
	if err != nil {
		color.Red.Println("Errore nel creare: %v", err)
	}
	jpeg.Encode(third, image3, &jpeg.Options{75})
	defer third.Close()
}

func main() {
	// Check del file di configurazione
	filename, _ := filepath.Abs("config/config.yml")
	yamlFile, _ := ioutil.ReadFile(filename)
	var config Config
	err := yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		panic(err)
	}

	args := os.Args[1:]

	length := len(args)

	// Se non ci sono parametri ritorna errore
	if len(args) < 1 {
		color.Green.Println(help)
		return
	}

	// Se il primo argomento è help ritorna messaggio di help
	if args[0] == "help" {
		color.Green.Println(help)
		return
	}
	switch length {
	case 1:
		fmt.Println(args[0], config.Logo, config.OutputDir)
		//fmt.Println("Un argomento applica logo default")
		// calcola dimensione foto
		xDim, yDim := calculateDimension(args[0])                   // Dimensioni foto
		maxDim := calculateMaxLogoDimension(xDim, yDim)             // Dimensioni massima foto
		logoXDim, logoYDim := calculateDimension(config.Logo)       // Dimensione logo
		checkDim := verifyLogoDimension(logoXDim, logoYDim, maxDim) // Verifica dimensioni logo 1/3 della foto
		if checkDim {                                               // Se logo ok
			lxDim, lyDim := calculatelogoPosition(xDim, yDim, logoXDim, "tr")  // Calcola dove posizionare il logo
			addWaterMark(args[0], config.Logo, config.OutputDir, lxDim, lyDim) //Apllica logo
		} else { // Se logo non ok
			//color.Blue.Println("DEBUG: faccio il resize del logo")
			resizeImage(config.Logo, maxDim) // Resize logo
			lxDim, lyDim := calculatelogoPosition(xDim, yDim, maxDim, "tr")
			addWaterMark(args[0], "./tmp/resized_logo.png", config.OutputDir, lxDim, lyDim) // Calcola posizione
		}

		//fmt.Println(lxDim, lyDim)
	case 2:
		fmt.Println(args[0], config.Logo, config.OutputDir)
		//fmt.Println("Un argomento applica logo default")
		// calcola dimensione foto
		xDim, yDim := calculateDimension(args[0])                   // Dimensioni foto
		maxDim := calculateMaxLogoDimension(xDim, yDim)             // Dimensioni massima foto
		logoXDim, logoYDim := calculateDimension(config.Logo)       // Dimensione logo
		checkDim := verifyLogoDimension(logoXDim, logoYDim, maxDim) // Verifica dimensioni logo 1/3 della foto
		if checkDim {                                               // Se logo ok
			lxDim, lyDim := calculatelogoPosition(xDim, yDim, logoXDim, args[1]) // Calcola dove posizionare il logo
			addWaterMark(args[0], config.Logo, config.OutputDir, lxDim, lyDim)   //Apllica logo
		} else { // Se logo non ok
			//color.Blue.Println("DEBUG: faccio il resize del logo")
			resizeImage(config.Logo, maxDim) // Resize logo
			lxDim, lyDim := calculatelogoPosition(xDim, yDim, maxDim, args[1])
			addWaterMark(args[0], "./tmp/resized_logo.png", config.OutputDir, lxDim, lyDim) // Calcola posizione
		}
	case 3:
		//fmt.Println("Tre valori, logo posizione e logo nuovo")
		// Calcolo le dimensioni del logo nuovo
		xDim, yDim := calculateDimension(args[0])         // Dimensioni foto
		maxDim := calculateMaxLogoDimension(xDim, yDim)   // Dimensioni massima foto
		logoXDim, logoYDim := calculateDimension(args[2]) // Dimensione logo
		checkDim := verifyLogoDimension(logoXDim, logoYDim, maxDim)
		if checkDim { // Se logo ok
			lxDim, lyDim := calculatelogoPosition(xDim, yDim, logoXDim, args[1]) // Calcola dove posizionare il logo
			addWaterMark(args[0], args[2], config.OutputDir, lxDim, lyDim)       //Apllica logo
		} else { // Se logo non ok
			//color.Blue.Println("DEBUG: faccio il resize del logo")
			resizeImage(args[2], maxDim)                                                    // Resize logo
			lxDim, lyDim := calculatelogoPosition(xDim, yDim, maxDim, args[1])              // Calcola posizione
			addWaterMark(args[0], "./tmp/resized_logo.png", config.OutputDir, lxDim, lyDim) // Applica logo
		} // Verifica dimensioni logo 1/3 della foto
	}
}
